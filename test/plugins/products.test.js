import Server from '../../src'

describe(`Products Plugin`, () => {

  let server
  beforeEach(async () => {
    server = await Server()
  })

  it(`/v1/products returns an empty array by default`, async () => {
    const { statusCode, result } = await server.inject('/v1/products')
    expect(statusCode).to.be.equal(200)
    expect(result).to.be.an.array()
  })


})
