import { Server } from 'hapi'
import ProductsPlugin from '../../src/plugins/products'
import PersistencePlugin from '../../src/plugins/persistence'

const PLUGINS = [
  { plugin: ProductsPlugin },
  { plugin: PersistencePlugin },
]

describe(`Products Plugin`, () => {





  describe(`Dependency Loading`, () => {

    it(`plugin didn't wait for it's dependencies to load`, async () => {

      try {
        const server = new Server()
        await server.register(PLUGINS[0])
        await server.initialize()
        throw Error('WTF?')
      } catch(err) {
        expect(err.message).to.be.match(/Plugin/)
      }

    })

  })

  let server
  beforeEach(async () => {
    server = new Server()
    await server.register(PLUGINS)
    await server.initialize()
  })



  it(`/v1/products returns an empty array by default`, async () => {
    const { statusCode, result } = await server.inject('/v1/products')
    expect(statusCode).to.be.equal(200)
    expect(result).to.be.an.array()
  })

})
