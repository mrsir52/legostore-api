import Monk from 'monk'
import pkg from './package'

export default {
  pkg,
  register(server, options ={}) {
    const { uri = 'localhost:32768/legostore' } = options
    const db = Monk(uri)

    server.expose({
      db
    })
  }
}
